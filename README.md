# dyaml-poc

Quick and dirty dyaml parser PoC for sequence of mappings.

Hardcoded to read and attempt to parse `input.yml` for convenience.

Can dump to `output.yml` if desired.

## usage

Run `dub run` in the root of the project (assumes that `ldc2` has been installed and configured).

Binary will be named `dyaml-poc`.
