# stone.yml schema / static typing parsing proposal

This is an attempt at documenting the thoughts behind the creation of a reasonably generic and useful set of schema types for the Serpent OS `stone.yml` recipe format, which are to be backed by struct definitions and enforced via template meta-programming, type introspection and traits-based contracts.

The goal of this exercise is to be able to easily annotate yml fields with expected types in order to be able to lint `stone.yml` when using `boulder`.

The primary benefit is being able to explain succintly why parsing an erroneous value will not work and what a correct value looks like, hopefully making it easier to discover the recipe format in the process.

A secondary benefit is to make it appreciably harder to introduce malicious code in the yml, which is of particular concern when having a source-based recipe repository.

## OptionallyTypedScalar definition

Using the proposed `paths:` sequence key as an example:

```YAML
packages:
    - "%(name)-foo":
        paths:
            - /usr/lib/*.so : symlink
            - /usr/share/%(name)/foo/ : directory
            - /user/share/%(name)/foo*.example # : any is the default
```

Based on the above, it should be possible to specify that `paths` is expected to be a sequence of OptionallyTypedScalar values.

An OptionallyTypedScalar _MUST_ be specified as:

    some_value : some_type

XOR

    some_value

The second form implies the default `: some_type` type.

Obviously, for this to make sense, an OptionallyTypedScalar _MUST_ define an associated enum detailing the legal values of its type parameter. The default type parameter (which matches when no type is explicitly set via the `some_value : some_type` format) _MUST_ be the first enum type listed.

### EXAMPLE:

    enum PathMatch : string
    {
        /* The first enum entry is the default when no type is specified */
        PathMatch = "any";
        PathMatch = "directory";
        PathMatch = "regular";
        PathMatch = "symlink";
    }

Parsing algorithm for OptionallyTypedScalar:

    IF ' : ' is part of the scalar, split into <value-field> ' : ' <type-field>
    THEN
        - Cast the value-field to the expected type
          - IF the value-field cannot be cast to the expected type, THEN abort with a suitable error.
        - Cast the type-field to the specified enum type
          - IF the value-field cannot be cast to the expected enum type, THEN abort with a suitable error.
    ELSE
        - Cast the value-field to the expected type
          - IF the value-field cannot be cast to the expected type, THEN abort with a suitable error.

### Stretch Goal:

    Implement a fuzzy match that recognises typos and asks "did you mean foo?" during linting.

## TypedURI definition

From a YAML perspective, a TypedURI is simply a scalar.  However, by declaring that a scalar is a TypedURI (probably via an annotation), we can still lint it nicely.

The format is similar to the `OptionallyTypedScalar`, in that it has a default type of plain in the following format:

    <type>|<uri> : <type-specific-qualifier>

For `git|` (& friends) it will be slightly more involved to lint the format, as the <type-specific-qualifier> field needs to be passed to a fetcher, which means that it's probably still desirable to match it against known valid patterns (source of exploits otherwise).

### EXAMPLE:

    upstreams:
        - plain|https://serpentos.com/downloads/moss/moss-%(version).tar.zst : <supported-hash-type-guessed-based-on-length>
        # here, the default plain| prefix is omitted for convenience
        - https://serpentos.com/downloads/moss/moss-%(version).tar.zst : <supported-hash-type-guessed-based-on-length>
        - git|https://gitlab.com/serpent-os/core/moss.gitlab : v%(version) # git tag is supported
        - git|https://gitlab.com/serpent-os/core/moss.gitlab : <short-enough-to-be-unique-git-hash> # also supported
        - cpan|<some-CPAN-pacakage> : <hash> # makes it possible to manage CPAN macros and URIs (pypi, gems etc.) centrally
        - (...)

    enum UpstreamType : string
    {
        Plain "plain",
        Git = "git",
        SourceForge = "sf",
        Mercurial = "hg",
        PyPi = "pypi",
        CPAN = "cpan",
        CTAN = "ctan",
        RubyGem = "gem",
        LuaRock = "rock";
        // TODO: Update with more relevant examples
    }

    // one way of handling hash types?
    enum HashType : uint8_t
    {
        // use the hex string length to look up supported type
        // if the length is not known, coerce to Invalid (0)
        Invalid = 0;
        SHA1Sum = 32;
        SHA256Sum = 64;
        SHA512Sum = 128;
    }

## TypedRecord definition

YAML supports a concept which they specify as "(Maps) Nested in a sequence", which is a convenient shorthand way of specifying several records with key : value parameters.

The proposed `TypedRecord` takes such a sequence map and parses it based on the first key it meets in each record in the sequence.

### EXAMPLE

    upstreams:
        - git   : https://gitlab.com/serpent-os/core/moss.gitlab
          ref   : v%(version)
        - plain : https://serpentos.com/downloads/moss/moss-%(version).tar.zst
          hash  : <somehash>

Above, there are two TypedRecords; the first is of type `UpstreamType.Git` and the second is of type `UpstreamType.Plain`.

Here, the UpstreamType determines which other keys are valid and how to parse them. In the case of git, `ref` is the only valid key and must have a non-empty string value, which is a valid git ref, such as a tag string or a unique-enough hash.

In the case of plain, `hash` is the only valid key and must have a non-empty string value, which can be successfully cast to a known hash type (this field is of TypedScalar).

