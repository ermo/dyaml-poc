/* SPDX-License-Identifier: Zlib */

/**
 * dyaml-poc
 *
 * PoC for parsing a hypothetical upstreams: YAML record format
 *
 * Authors: © 2020-2022 Serpent OS Developers
 * License: ZLib
 */
module FIXME;

import std.array;
import std.conv;
import std.format;
import std.stdio;
// import std.typecons : Nullable;
import dyaml;

void main()
{
	/* Read the input */
	writeln("\nParsing input.yml for valid upstream records...");
	Node root = Loader.fromFile("input.yaml").load();

	/* Parse upstream node */
	///
	/// Example:
	///
	/// upstreams:
	///    - git    : someurl
    ///      ref    : someref
	///    - plain  : someurl
	///      hash   : somehash
	///
	///  Notes:
	///  - duplicate keys in a record causes a YAML parser exception which needs to be caught.
	///  - this suggests that perhaps throwing the same kind of exception with a pertinent
	///    message is a good way to go in terms of wrapping exceptions and turning them into
	///    linter results?
	auto upstreams = root["upstreams"];

	size_t i = 0;
	foreach(Node upstream; root["upstreams"])
	{
		/* We need to be dealing with a legit sequence of mappings here */
		writefln("\n> YAML upstream node #%d:\n\n%s", i, upstream);
		++i;
		// Essentially a linear search.
		//
		// Probably need a a list of pairs to match in order until
		// we find a valid result. The good news is that the linear search bails on a failed O(1)
		// lookup per type due to how conditions are evaluated.
		//
		// Using auto here covers the null case for now
		//
		writeln("\n>> Checking for git upstream...");
		auto git = getFetchable(upstream, "git", "ref",
								"A valid git upstream record is of the form\n- git: <uri>\n  ref: <git tag or commit hash>");
		if (git != "")
		{
			writeln(git);
			continue;
		}

		writeln(">> Checking for plain upstream...");
		// this is just a PoC, remember?
		auto plain = getFetchable(upstream, "plain", "hash",
								  "A valid plain upstream record is of the form\n- plain: <uri>\n  hash: <checksum value>");
		if (plain != "")
		{
			writeln(plain);
			continue;
		}
	}
	/* Dump the loaded document to output.yaml */
	// dumper.dump(File("output.yaml", "w").lockingTextWriter, root);
}

/* FIXME: Work out an actually good return value concept here? */
string getFetchable(Node _upstream, string type, string val, string lintMsg)
{
	string result = "";

	/* PoC validator code w/trivial linter for when validation fails */
	if (_upstream.containsKey(type))
	{
		writefln(">>> Linting %s upstream stanza...", type);
		if(_upstream.containsKey(val) && _upstream.length == 2)
		{
			string _uri = _upstream[type].get!string;
			string _val = _upstream[val].get!string;
			result = "%sFetchable(%s, %s)".format(type, _uri, _val);
			writefln(">>>> Found %s upstream: (%s, %s)", type, _uri, _val);
		}
		else
		{
			// TODO: We could add line number?
			writeln(">>>> Erroneous upstream stanza:");
			dumpNode(_upstream);
			writefln(">>>> ERROR: %s", lintMsg);
		}
	}

	return result;
}

/**
 * Convenience wrapper for dumping a node to a debug channel for the user to see
 */
void dumpNode(Node node)
{
	auto stream = appender!string();
	auto dumper = dumper();
	dumper.indent = 4;
	dumper.defaultScalarStyle = ScalarStyle.singleQuoted;
	// dumper.defaultCollectionStyle = CollectionStyle.flow;
	dumper.dump(stream, node);
	/* write the currently in-memory stream to an actual output -- it's pretty ugly (PoC!) */
	writeln(stream[]);
}
